docker rmi $(docker images -qa -f 'dangling=true')
docker rm libotrend

docker build -t libotrend .

docker volume create -d local \
    -o device=/home/libo/git/liboch/main/playground/trend_analyzer/data \
    -o o=bind \
    -o type=none \
    vol.data

#docker volume create -d local \
#    -o device=/home/libo/git/liboch/main/playground/trend_analyzer/grafana \
#    -o o=bind \
#    -o type=none \
#    vol.grafana

docker run -v vol.data:/data:rw --network trend-net --name libotrend libotrend
#docker run -it --rm -v vol.db:/db:rw --name libotrend libotrend

