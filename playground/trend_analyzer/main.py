from lib.twitter import Twitter
from lib.storage import Storage
from lib.targets import Targets
from lib.grafana import Grafana

import logging
import json
import time
from pprint import pformat
from pprint import pprint


import os

def setLoggingProperties():
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s')

    logging.getLogger('').setLevel(logging.DEBUG)
#
#    fh = logging.FileHandler('trend.log', mode='a', encoding='utf-8')
#    fh.setLevel(logging.DEBUG)
#    fh.setFormatter(formatter)
#    logging.getLogger('').addHandler(fh)
#
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logging.getLogger('').addHandler(ch)


def main():
    logger = logging.getLogger(__name__)
    logger.debug(f"Starting main")

    with open("/data/config.json", "r") as jsonfile:
        config = json.load(jsonfile)
    logger.debug(f'Config:\n{pformat(config)}')

    with open("/app/secrets.json", "r") as jsonfile:
        secrets = json.load(jsonfile)
    logger.debug(f'Secrets:\n{pformat(secrets)}')


    targets = Targets()
    storage = Storage()
    grafana = Grafana(config['grafana'])
    grafana.recreateDashboard(targets.targetsList['targets'])

    x = Twitter(secrets["channels"]["twitter"])

    while True:
        targets.loadTempFile("/data/list")
        targets.saveTargets()
        x.collect(targets,storage);
        logger.info(f"Sleeping for 12h")
        time.sleep(43200)



if __name__ == '__main__':
    setLoggingProperties()
    main()

