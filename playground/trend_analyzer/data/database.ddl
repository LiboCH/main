CREATE TABLE trends(
  date text,
  time text, 
  channel text, 
  target text,
  metric text,
  value integer
);
