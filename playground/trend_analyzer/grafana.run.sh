docker run -d \
  -p 3000:3000 \
  --name grafana \
  --network trend-net \
  -e "GF_INSTALL_PLUGINS=frser-sqlite-datasource" \
  -v vol.data:/data:rw \
  grafana/grafana-oss:latest-ubuntu
