import logging
import json
from pprint import pprint
from lib.grafana import Grafana


formatter = logging.Formatter('%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s')
logging.getLogger('').setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logging.getLogger('').addHandler(ch)

grafana = Grafana()
grafana.loadTemplate()
