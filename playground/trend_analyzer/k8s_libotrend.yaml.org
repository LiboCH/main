apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: libotrend
  name: libotrend
spec:
  containers:
    - image: n1:30443/libotrend:0.1
      name: libotrend
      volumeMounts:
        - name: libotrend-vol
          mountPath: "/data"
  imagePullSecrets:
    - name: private-registry
  volumes:
    - name: libotrend-vol
      persistentVolumeClaim:
          claimName:  nas-pvc

