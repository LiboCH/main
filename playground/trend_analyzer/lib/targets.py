import logging
import os
import shutil
import json
from datetime import datetime
from pprint import pprint
from pprint import pformat

logger = logging.getLogger(__name__)


class Targets(object):
    def __init__(self):
        self.targetsFile="/data/targets.json"
        self.targetsList= {}
        self.targetsList['targets']= {}
        if os.path.exists(self.targetsFile):
            logger.debug(f"Targets file exists: {self.targetsFile}")
            with open(self.targetsFile, "r") as jsonfile:
                self.targetsList = json.load(jsonfile)
                logger.debug(pformat(self.targetsList))
                logger.info(f"Targets loaded: {len(self.targetsList['targets'])}")
        else:
            logger.debug(f"Targets file does not exists: {self.targetsFile}")

    def saveTargets(self):
        #Saves content of self.targetsLlist to file
        dt = datetime.now()
        backupFile=f"/data/archive/{os.path.basename(self.targetsFile)}_{dt.strftime('%Y%d%m_%H%M%S')}"
        try:
            os.mkdir("/data/archive")
            os.chmod("/data/archive",0o0777)
        except:
            pass

        if os.path.exists(self.targetsFile): shutil.copy(self.targetsFile,backupFile)

        with open(self.targetsFile, 'w') as jsonfile:
            json.dump(self.targetsList, jsonfile, indent=4, sort_keys=True)
        os.chmod(backupFile,0o0666)
        logger.info(f"Targets save to: {self.targetsFile}, backup in archive: {backupFile}")

    def processTempBlock(self,block):
        #parses the link list from temp file to targest structure
        twitterUser=""
        for link in block:
            if "//twitter.com/" in link:
                twitterUser=link.split("/")[-1]
                if not twitterUser in self.targetsList['targets']: self.targetsList['targets'][twitterUser] = {}
                if not 'twitter' in self.targetsList['targets'][twitterUser]: self.targetsList['targets'][twitterUser]['twitter']= {}
                self.targetsList['targets'][twitterUser]['twitter']['userName']=twitterUser
                self.targetsList['targets'][twitterUser]['twitter']['id']="tbd"
                logger.debug(f"Twitter link found: {link}, twitter user: {twitterUser}")
            elif "//t.me/" in link:
                if not twitterUser in self.targetsList['targets']:
                    logger.warn("Twitter Username not defined for current local block, in tmp list twitter link should be on first place")
                else:
                    if not 'telegram' in self.targetsList['targets'][twitterUser]: self.targetsList['targets'][twitterUser]['telegram']= {}
                    self.targetsList['targets'][twitterUser]['telegram']['userName']=link.split("/")[-1]
                    logger.debug(f"Telegram link found: {link}, telegram user: {link.split('/')[-1]}")
            elif "//discord.com/" in link:
                if not twitterUser in self.targetsList['targets']:
                    logger.warn("Twitter Username not defined for current local block, in tmp list twitter link should be on first place")
                else:
                    if not 'discord' in self.targetsList['targets'][twitterUser]: self.targetsList['targets'][twitterUser]['discord']= {}
                    self.targetsList['targets'][twitterUser]['discord']['userName']=link.split("/")[-1]
                    logger.debug(f"Discord link found: {link}, discord user: {link.split('/')[-1]}")

    def loadTempFile(self,tmpFile):
        if not os.path.exists(tmpFile):
            logger.info(f"Local tmp file does not exists: {tmpFile}, skipping");
            return
        block = []
        with open(tmpFile,'r') as f:
            lines = f.readlines()
            for line in lines:
                logger.debug(f"Line read from file: {line.rstrip()}")
                if line.rstrip() == "":
                    logger.debug("Process new block")
                    logger.debug(f"\n{pformat(block)}")
                    self.processTempBlock(block)
                    block=[]
                else:
                    block.append(line.rstrip())

