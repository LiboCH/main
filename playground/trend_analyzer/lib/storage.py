import logging
import sqlite3 as sl
from datetime import datetime
import time
logger = logging.getLogger(__name__)


class Storage:
    def __init__(self):
        self._con = sl.connect('/data/LiboTrends.sqlite')

    def __del__(self):
        self._con.close()

    def store(self,target,channel,metric,value):
        logger.debug(f"In storage, target:{target}, channel:{channel}, metric:{metric}, value:{value}")
        dt = datetime.now()

        statement = f"insert into trends values ('{dt.strftime('%Y-%d-%mT%H:%M:%SZ')}','{int(time.time())}','twitter','{target}','{metric}',{value})"
        logger.debug(f"{statement}")
        self._con.execute(statement)
        self._con.commit()


