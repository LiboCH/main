import logging
import json
import os
import requests
from requests.auth import HTTPBasicAuth
from pprint import pformat
from pprint import pprint

logger = logging.getLogger(__name__)


class Grafana:
    def __init__(self,config):
        self.config = config
        self._apiUrl = f"{self.config['url']}api"
        self._auth=HTTPBasicAuth(self.config['user'], self.config['password'])
        self._templateFile = f"/data/template_{self.config['dashboardUid']}.json"
        self.dashboard = ""
        self.targetTemplate = {}
#        with open(self._templateFile, "r") as jsonfile:
#            self.dashboard = json.load(jsonfile)


    def _get(self,url):
         req = self._apiUrl+url
         logger.debug(f"req: {req}")
         resp = requests.get(req, auth=self._auth)
         if resp:
             return resp
         else:
             logger.warning(f"Get request unsuccessful. Message: {resp.text}")
             return resp

    def _post(self,url,data):
        req = self._apiUrl+url
        headers = {"Content-type": "application/json", "Accept":"application/json"}
        logger.debug(f"In self._post, req: {req} data: {data}")
        resp=requests.post(req, data=data,headers=headers, auth=self._auth)
        if resp:
            return resp
        else:
            logger.warning(f"Post request unsuccessful. Message: {resp.text}")
            return resp


    def convertToTemplate(self):
        #if self.dashboard['panels'][0]['targets'][0] :
        if 'panels' in self.dashboard['dashboard'] and 'targets' in self.dashboard['dashboard']['panels'][0] :
            logger.info("self.dashboard has panels - OK")
            logger.debug(f"Template\n{pformat(self.dashboard['dashboard']['panels'][0]['targets'])}")
        else:
            logger.error("Unable to find panel in self.dashboard")

        #load first target from targets as template
        self.targetTemplate = self.dashboard['dashboard']['panels'][0]['targets'][0]
        self.dashboard['dashboard']['panels'][0]['targets'] = []
        self.dashboard['dashboard']['panels'][1]['targets'] = []

    def addTargetToDashboardFollowers(self,target):
        self.dashboard['dashboard']['panels'][0]['targets'].append(target.copy());

    def addTargetToDashboardTweets(self,target):
        self.dashboard['dashboard']['panels'][1]['targets'].append(target.copy());

    def recreateDashboard(self,targets):
        logger.info(f"Recreating Dashboard: {self.config['dashboardUid']}")
        resp = self._get(f"/dashboards/uid/{self.config['dashboardUid']}")
        self.dashboard = resp.json()
        self.convertToTemplate()
        i=65
        for target in targets:
            tGrafanaTarget=self.targetTemplate
            tGrafanaTarget['queryText']=f"select time,value as {target} from trends where target='{target}' and metric='followers_count'"
            tGrafanaTarget['rawQueryText']=f"select time,value as {target} from trends where target='{target}' and metric='followers_count'"
            tGrafanaTarget['refId'] = chr(i)
            self.addTargetToDashboardFollowers(tGrafanaTarget)

            tGrafanaTarget = self.targetTemplate
            tGrafanaTarget['queryText']=f"select time,value as {target} from trends where target='{target}' and metric='tweet_count'"
            tGrafanaTarget['rawQueryText']=f"select time,value as {target} from trends where target='{target}' and metric='tweet_count'"
            tGrafanaTarget['refId'] = chr(i)
            self.addTargetToDashboardTweets(tGrafanaTarget)
            i += 1

#        tmpFile="/data/out.json"
#        with open(tmpFile, 'w') as jsonfile:
#            json.dump(self.dashboard, jsonfile, indent=4, sort_keys=True)
#        os.chmod(tmpFile,0o0666)

        self._post("/dashboards/db",json.dumps(self.dashboard))



# 2023  curl -u admin:admin http://localhost:3000/api/dashboards/uid/
# 2024  curl -u admin:admin http://localhost:3000/api/dashboards/db
# 2025  curl -u admin:admin http://localhost:3000/api/dashboards/
# 2026  curl -u admin:admin http://localhost:3000/api/dashboards/uid
# 2027  curl -u admin:admin http://localhost:3000/api/dashboards/jhome
# 2028  curl -u admin:admin http://localhost:3000/api/dashboards/home
# 2029  curl -u admin:admin http://localhost:3000/api/dashboards/home | python -m json
# 2030  curl -u admin:admin http://localhost:3000/api/dashboards/home | json_pp 
# 2031  curl -u admin:admin http://localhost:3000/api/dashboards/tags | json_pp 
# 2032  curl -u admin:admin http://localhost:3000/api/dashboards/uid/udQnRox7k | json_pp 
# 2033  curl -u admin:admin http://localhost:3000/api/dashboards/uid/udQnRox7k | json_pp  > a.json
# 2034  curl -u admin:admin -X POST -H "Content-Type: application/json" -d @a.json http://localhost:3000/api/dashboards/db
# 2035  curl -u admin:admin http://localhost:3000/api/dashboards/uid/udQnRox7k | json_pp  > a.json
# 2036  curl -u admin:admin -X POST -H "Content-Type: application/json" -d @a.json http://localhost:3000/api/dashboards/db
# 2037  curl -u admin:admin http://localhost:3000/api/dashboards/uid/udQnRox7k | json_pp  > a.json
#
