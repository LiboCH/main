import logging
import requests
from pprint import pprint

logger = logging.getLogger(__name__)


class Twitter(object):
    def __init__(self, secrets):
        self._apiUrl="https://api.twitter.com/2"
        self._headers = {}
        self._headers["Authorization"] = f"Bearer {secrets['bearer']}"

    def _get(self,url):
        req = self._apiUrl+url
        logger.debug(f"req: {req}")
        resp = requests.get(req, headers=self._headers)
        if resp:
            return resp
        else:
            logger.warning(f"Get request unsuccessful. Message: {resp.text}")
            return resp

    def collect(self,targets,storage):
        for twitterUserName in targets.targetsList['targets']:
            logger.debug(f"Iteragint over targets, element: {twitterUserName}")
            if "twitter" in targets.targetsList['targets'][twitterUserName]:
                logger.info(f"collecting data for twitter user: {targets.targetsList['targets'][twitterUserName]['twitter']['userName']}")
                resp = self._get(f"/users/by/username/{targets.targetsList['targets'][twitterUserName]['twitter']['userName']}?user.fields=public_metrics").json()
                storage.store(targets.targetsList['targets'][twitterUserName]['twitter']['userName'],"twitter","followers_count",resp['data']['public_metrics']['followers_count'])
                storage.store(targets.targetsList['targets'][twitterUserName]['twitter']['userName'],"twitter","tweet_count",resp['data']['public_metrics']['tweet_count'])

#    def _post(self,url,data):
#        req = self._url+url
#        headers = {"Content-type": "application/json", "Accept":"application/json"}
#        logger.debug(f"In self._post, req: {req} data: {data}")
#        resp=requests.post(req, data=data,headers=headers, auth=(self._user,self._password))
#        if resp:
#            return resp
#        else:
#            logger.warning(f"Post request unsuccessful. Message: {resp.text}")
#            return resp
#
#    def _put(self,url,data):
#        req = self._url+url
#        headers = {"Content-type": "application/json", "Accept":"application/json"}
#        logger.debug(f"In self._put, req: {req} data: {data}")
#        resp=requests.put(req, data=data,headers=headers, auth=(self._user,self._password))
#        if resp:
#            return resp
#        else:
#            logger.warning(f"Put request unsuccessful. Message: {resp.text}")
#            return resp
#
#    def _delete(self,url):
#        req = self._url+url
#        headers = {"Content-type": "application/json", "Accept":"application/json"}
#        logger.debug(f"In self._delete, req: {req}")
#        resp=requests.delete(req,headers=headers, auth=(self._user,self._password))
#        if resp:
#            return resp
#        else:
#            logger.warning(f"Delete request unsuccessful. Message: {resp.text}")
#            return resp

