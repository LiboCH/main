#!/usr/bin/env python

#
#  MFT_RERUN_NEWER_THAN  - bash variable, changes the default range of log files to load, default 3 days
#


import glob
import os
from datetime import datetime
import pprint
import curses
from curses.textpad import rectangle
import re
import threading
import time
import random
import subprocess
#from operator import itemgetter
os.environ['NCURSES_NO_UTF8_ACS']='1'


fileList = {}


class Object:
    def __init__(self):
        self.objectList=['files','search', 'confBox']
        self.active=0
        self.searchString=''
    def setActiveNext(self):
        if self.active != 2:
            self.active = ( self.active + 1 ) % (len(self.objectList) - 1)

    def setActiveObject(self,p_search, p_files, p_confBox):
       if self.objectList[self.active] == 'files':
            p_confBox.setInactive()
            p_search.setInactive()
            p_files.setActive()
       elif self.objectList[self.active] == 'search':
            p_confBox.setInactive()
            p_files.setInactive()
            p_search.setActive()
       else:
            p_confBox.setActive()
            p_files.setInactive()
            p_search.setInactive()

    def getActive(self):
        return self.objectList[self.active]

    def processButton(self, p_buttonPressed,p_stdscr, p_fileFilst, p_confBox ):
        if p_buttonPressed == ord ('\t') : self.setActiveNext()
        if p_buttonPressed == 10 and self.active == 0 :
            self.active = 2
        elif p_buttonPressed == 10 and self.active == 2 and p_confBox.activeBox=='PendingBox':
            if p_confBox.selectedButton == 1:
                self.active = 0
                p_confBox.activeBox='None'
            else:
                p_confBox.activeBox='RunningBox'
                p_confBox.drawRunning(p_stdscr, p_fileFilst,self);

class fileList():
    def __init__(self,p_position,p_logDirectory):
        self.x,self.y=p_position
        self.logDirectory=p_logDirectory
        self.fileList = []
        self.visibleList = []
        self.is_active = True
        self.selectedItem = 0
        self.startWindow=0
        self.windowStart=0
        self.windowEnd=20
        self.loadLogs()


    def loadLogs(self):
        t_fileListIndex=len(self.visibleList)
        t_fileList=glob.glob(self.logDirectory)
        t_now = time.time()
        t_file_newer = int(os.getenv('MFT_RERUN_NEWER_THAN', '3'))


        for t_file in t_fileList:
            if os.stat(t_file).st_mtime < t_now - t_file_newer * 86400:
                #os.system("echo not processing " + t_file + " >> /tmp/log")
                continue
            #os.system("echo " + t_file + " >> /tmp/log")
            #os.system("echo " + str(os.stat(t_file).st_mtime ) + " >> /tmp/log")
            if not any(d.get('fileName',None) == os.path.basename(t_file) for d in self.fileList ):
                t_currentStatus='SUCCESS'
                t_fileH = open(t_file,"r")
                t_blocks=[]
                t_blockName=''
                t_blockStatus='NO ERROR'
                t_rerun =''
                for t_line in t_fileH:
                    if re.search(r'--external-config',t_line) : t_rerun = ' (rerun)'
                    if re.search('/cmd/mft_common.pl',t_line) : t_command = re.sub(r'--external-config.*','',t_line.split('INFO:')[1].rstrip("\n") )
                    if re.search('WARN',t_line) and t_currentStatus=='SUCCESS': t_currentStatus='SUCCESS (warn)'
                    if re.search('ERROR',t_line) :
                        t_currentStatus='ERROR'
                        t_blockStatus='ERROR'
                    if re.search('FATAL',t_line) :
                        t_currentStatus='FATAL'
                    #for old and new log comment
                    if re.search('Checking for configruation file:', t_line) : t_configFile =  t_line.split('file:')[1]
                    if re.search('Using configuraion file:', t_line) : t_configFile =  t_line.split('file:')[1]


                    if re.search('In check flow for config block', t_line):
                        if t_blockName != '':
                            t_blocks.append({'blockName':t_blockName,
                                           'blockStatus':t_blockStatus,
                                            'blockRerun':False if t_blockStatus != 'ERROR' else True,
                                           'blockConfig':[]})

                        t_blockName = t_line.split('block: ')[1]
                        t_blockStatus = 'NO ERROR'
                if t_blockName != '':
                    t_blocks.append({'blockName':t_blockName,
                                   'blockStatus':t_blockStatus,
                                    'blockRerun':False if t_blockStatus != 'ERROR' else True,
                                   'blockConfig':[]})

                self.fileList.append(     {'fileName':os.path.basename(t_file),
                                                'dir':os.path.dirname(t_file),
                                           'fileType':os.path.basename(t_file).split('.')[1],
                                         'createTime':datetime.fromtimestamp(os.path.getmtime(t_file)).strftime('%Y-%m-%d %H:%M:%S'),
                                         'configFile':t_configFile.lstrip().rstrip("\n"),
                                           'isMarked':False,
                                          'isVisible':True,
                                            'command':t_command,
                                      'currentStatus':t_currentStatus + t_rerun,
                                      'commandStatus':'none',
                                         'sortString':datetime.fromtimestamp(os.path.getmtime(t_file)).strftime('%Y%m%d%H%M%S')+os.path.basename(t_file).split('.')[1],
                                             'blocks':sorted(t_blocks, key=lambda item: item['blockName'], reverse=False),
                                     'isConfigLoaded':False
                                      })
                self.visibleList.append(t_fileListIndex)
                t_fileListIndex +=1

        self.fileList=sorted(self.fileList , key=lambda item: item['sortString'], reverse=True)



    def setActive(self):
        self.is_active=True
    def setInactive(self):
        self.is_active=False

    def setCommandStatus(self, p_status, p_i):
        self.fileList[p_i].update({'commandStatus':p_status})

    def getSelectedCount(self):
        t_count=0
        for i in range(len(self.fileList)):
            if self.fileList[i]['isMarked'] : t_count+=1
        return t_count

    def getSelectedCountWithBlocks(self):
        t_count=0
        for i in range(len(self.fileList)):
            if self.fileList[i]['isMarked'] :
                t_count+=1
                t_count+= len(self.fileList[i]['blocks'])
        return t_count

    def unmarkAll(self):
        for i in range(len(self.fileList)):
            self.fileList[i].update({'isMarked':False})

    def selectUnselectBlock(self, p_toMark):
        t_i = p_toMark[0]
        t_blockName = p_toMark[1]
        for t_block in self.fileList[t_i]['blocks']:
            if t_block['blockName'] == t_blockName:
                t_block['blockRerun'] = False if t_block['blockRerun'] else True

    def loadConfig(self,p_i ):
        if self.fileList[p_i]['isConfigLoaded'] : return
        try:
            t_configFileH=open(self.fileList[p_i]['configFile'],"r")
        except FileNotFoundError:
            return
        for t_configFileLine in t_configFileH:
            for block in self.fileList[p_i]['blocks']:
                if re.search(r''+block['blockName'].rstrip()+'\.[a-zA-Z]',t_configFileLine): block['blockConfig'].append(t_configFileLine)
        self.fileList[p_i]['isConfigLoaded']=True


    def draw(self, p_stdscr):
        t_pos=0
        for key,i  in enumerate(self.visibleList):
            if key >= self.windowStart and key <= self.windowEnd :
                t_markString="[X] " if self.fileList[i]['isMarked'] else "[ ] "
                t_selectColor = curses.color_pair(2) if key==self.selectedItem and self.is_active else curses.color_pair(1)
                if key==self.selectedItem and self.is_active:
                    if re.match('SUCCESS',self.fileList[i]['currentStatus']):
                        t_statusSelectColor = curses.color_pair(52)
                    elif re.match('ERROR|FATAL',self.fileList[i]['currentStatus']):
                        t_statusSelectColor = curses.color_pair(42)
                else:
                    if re.match('SUCCESS',self.fileList[i]['currentStatus']):
                        t_statusSelectColor = curses.color_pair(5)
                    elif re.match('ERROR|FATAL',self.fileList[i]['currentStatus']):
                        t_statusSelectColor = curses.color_pair(4)

                p_stdscr.addstr(self.y+t_pos, self.x , "{} {:12s}    {}  ".format(t_markString,self.fileList[i]['fileType'],self.fileList[i]['createTime']), t_selectColor)
                p_stdscr.addstr("{:20s}".format(self.fileList[i]['currentStatus']), t_statusSelectColor)
                t_pos+=1

    def processButton(self,p_buttonPressed):
        if self.is_active:
            if p_buttonPressed == curses.KEY_DOWN:
                if self.selectedItem + 1 < len(self.visibleList):
                    if self.selectedItem == self.windowEnd:
                        self.windowEnd += 1
                        self.windowStart +=1
                    self.selectedItem+=1

            elif p_buttonPressed == curses.KEY_UP:
                if self.selectedItem > 0  :
                    if self.selectedItem == self.windowStart:
                        self.windowEnd -= 1
                        self.windowStart -=1
                    self.selectedItem -= 1

            elif p_buttonPressed  == ord(' '):
                    self.fileList[self.visibleList[self.selectedItem]]['isMarked'] = True if self.fileList[self.visibleList[self.selectedItem]]['isMarked'] == False else False
            elif p_buttonPressed == 338: #pgdown
                t_windowSize=(self.windowEnd - self.windowStart)
                if self.selectedItem + t_windowSize < len(self.visibleList) :
                    self.selectedItem += t_windowSize
                    self.windowStart += t_windowSize
                    self.windowEnd += t_windowSize
                else:
                    self.selectedItem = len(self.visibleList) -1
                    self.windowEnd = len(self.visibleList) - 1
                    self.windowStart =len(self.visibleList) - t_windowSize -1

            elif p_buttonPressed == 339: #pgup
                t_windowSize=(self.windowEnd - self.windowStart)
                if self.selectedItem - (self.windowEnd - self.windowStart)  >0: #len(self.visibleList):
                    self.selectedItem -= t_windowSize
                    self.windowStart -= t_windowSize
                    self.windowEnd -= t_windowSize
                else:
                    self.selectedItem=0
                    self.windowStart=0
                    self.windowEnd=t_windowSize

    def setVisible(self, o_searchString):
        self.visibleList=[]
        self.selectedItem=0
        for i in range(len(self.fileList)):
            if re.search(r'(?i)'+o_searchString, self.fileList[i]['fileType']) or not len(o_searchString) :
                self.visibleList.append(i)


class searchField():
    def __init__(self,p_position):
        self.x, self.y=p_position
        self.is_activ=False
        self.searchString=''

    def setActive(self):
        self.is_active=True
    def setInactive(self):
        self.is_active=False

    def draw(self, p_stdscr):
        t_selectColor = curses.color_pair(2) if self.is_active else curses.color_pair(1)
        p_stdscr.addstr(self.y, self.x, "Search: ", t_selectColor)
        p_stdscr.addstr(self.searchString, curses.color_pair(1))

    def processButton(self,p_buttonPressed):
        if self.is_active:
            if (( p_buttonPressed >= 97 and p_buttonPressed <= 122) or( p_buttonPressed >= 48 and p_buttonPressed <= 57)) : self.searchString += chr(p_buttonPressed)
            if p_buttonPressed == 263 : self.searchString = self.searchString[:-1]


class confirmationBox():
    def __init__(self, p_position):
        self.x, self.y = p_position
        self.prev_x = self.x + 60
        self.prev_y = self.y
        self.selectedButton = 1
        self.activeButtons = True
        self.is_active = True
        self.activeBox='None'
        self.selectedItem=0
        self.toMark=[]

    def setActive(self):
        self.activeBox='PendingBox'
        self.is_active=True
        self.firstActivation=True

    def setInactive(self):
        self.is_active=False


    def processButton(self, p_buttonPressed, p_objectList,p_fileList):
        if self.is_active and self.activeBox=='PendingBox':
            if p_buttonPressed == curses.KEY_DOWN and not self.activeButtons:
                self.selectedItem += 1
            if p_buttonPressed == curses.KEY_UP and self.selectedItem > 0:
                self.selectedItem -= 1
                if self.activeButtons : self.activeButtons = False
            if p_buttonPressed == ord(' ') and not self.activeButtons:
                p_fileList.selectUnselectBlock(self.toMark)

            if self.activeButtons and (p_buttonPressed == ord('\t') or  p_buttonPressed == 260 or p_buttonPressed == 261) :
                self.selectedButton  =  ( self.selectedButton + 1 ) % 2

    def draw(self, p_stdscr, p_fileList):
        if self.is_active and self.activeBox=='PendingBox':
            t_selectableCount= p_fileList.getSelectedCountWithBlocks() -  p_fileList.getSelectedCount();
            t_allPosCount=p_fileList.getSelectedCountWithBlocks()
            t_statusSelectColor = curses.color_pair(5)
            if self.activeButtons:
                self.selectedItem=t_selectableCount

            if self.selectedItem == t_selectableCount:
                self.activeButtons = True
            t_pos=0
            t_printedPos=0
            self.win = curses.newwin(3+ t_allPosCount ,30, self.y+1, self.x+1)
            rectangle(p_stdscr, self.y, self.x, self.y+4+ t_allPosCount, self.x+31)
            self.win.addstr(0,7,'Rerun delivery?')
            for i in range(len(p_fileList.fileList)):
                if p_fileList.fileList[i]['isMarked']:
                    p_fileList.loadConfig(i)
                    self.win.addstr(1+t_pos, 1, p_fileList.fileList[i]['fileType'])
                    self.win.addstr(1+t_pos, 14, '       PENDING', curses.color_pair(3))
                    t_pos +=1
                    for block in p_fileList.fileList[i]['blocks']:
                        if t_printedPos == self.selectedItem:
                            t_selectColor = curses.color_pair(2)
                            self.toMark=[i,block['blockName']]
                            self.drawPreviewWindow(p_stdscr,block['blockConfig'])
                        else:
                            t_selectColor = curses.color_pair(1)
                        self.win.addstr(1+t_pos, 9,'{:7s} {} '.format( block['blockName'].split(p_fileList.fileList[i]['fileType'])[1].rstrip(),'[X]' if block['blockRerun'] else '[ ]' ), t_selectColor)
                        if re.match('NO ERROR', block['blockStatus']):
                            t_statusSelectColor = curses.color_pair(5)
                        elif re.match('ERROR', block['blockStatus']):
                            t_statusSelectColor = curses.color_pair(4)

                        self.win.addstr(1+t_pos, 20 , block['blockStatus'],t_statusSelectColor )
                        t_pos +=1
                        t_printedPos += 1

            if self.activeButtons :
                self.win.addstr(1+t_allPosCount,5,' YES ', curses.color_pair(2-self.selectedButton))
                self.win.addstr(1+t_allPosCount,18,' NO ',  curses.color_pair(self.selectedButton+1))
            else :
                self.win.addstr(1+t_allPosCount,5,' YES ', curses.color_pair(1))
                self.win.addstr(1+t_allPosCount,18,' NO ',  curses.color_pair(1))


    def drawRunning(self,p_stdscr,p_fileList, p_object):
        if self.is_active and self.activeBox=='RunningBox':
            t_selCount= p_fileList.getSelectedCountWithBlocks()
            t_toProcess = t_selCount
            t_keepRunning=True
            t_pos=0
            t_threads=[]
            self.win = curses.newwin(3+t_selCount,30, self.y+1, self.x+1)
            rectangle(p_stdscr, self.y, self.x, self.y+4+t_selCount, self.x+31)
            for i in range(len(p_fileList.fileList)):
                if p_fileList.fileList[i]['isMarked']:
                    t = threading.Thread(target=worker, args=(p_fileList, i))
                    t_threads.append(t)
                    t.start()


            while t_keepRunning:
                t_keepRunning=False
                t_pos=0
                for i in range(len(p_fileList.fileList)):
                    if p_fileList.fileList[i]['isMarked']:
                        self.win.addstr(1+t_pos, 1, p_fileList.fileList[i]['fileType'])
                        if p_fileList.fileList[i]['commandStatus'] ==  'RUNNING':
                            t_keepRunning=True
                            self.win.addstr(1+t_pos, 14, '       RUNNING', curses.color_pair(6))
                        elif p_fileList.fileList[i]['commandStatus'] == 'SUCCESS (warn)':
                            self.win.addstr(1+t_pos, 14, 'SUCCESS (warn)', curses.color_pair(3))
                        elif p_fileList.fileList[i]['commandStatus'] == 'ERROR':
                            self.win.addstr(1+t_pos, 14, '         ERROR', curses.color_pair(4))
                        elif p_fileList.fileList[i]['commandStatus'] == 'SUCCESS':
                            self.win.addstr(1+t_pos, 14, '       SUCCESS', curses.color_pair(5))
                        t_pos += 1

                        for block in p_fileList.fileList[i]['blocks']:
                            self.win.addstr(1+t_pos, 9, block['blockName'].split(p_fileList.fileList[i]['fileType'])[1])
                            self.win.addstr(1+t_pos, 17 , '[X]' if block['blockRerun'] else '[ ]')
                            self.win.addstr(1+t_pos, 20 , ' N/A', curses.color_pair(1) )
                            t_pos +=1

                self.win.refresh()
                time.sleep(1)
            self.win.refresh()
            p_fileList.loadLogs()
            self.win.addstr(1+t_pos, 7, ' PRESS ANY KEY ', curses.color_pair(2))
            self.win.getch()
            p_object.active=0
            p_fileList.unmarkAll()
            p_stdscr.refresh()

    def drawPreviewWindow(self,p_stdscr,t_content):
        t_maxLen=len(max(t_content, key = len))
        self.win_prev = curses.newwin(4+ len(t_content) ,t_maxLen+4, self.prev_y, self.prev_x)
        self.win_prev.border()
        for i in range(len(t_content)):
            self.win_prev.addstr(2+i, 2, t_content[i].rstrip("\n"))



    def removePreviewWidnow(self,p_stdscr):
        try :
            pass
            #del self.win_prev
        except :
            pass

    def refresh(self):
        if self.is_active:
            self.win.refresh()
        try:
            self.win_prev.refresh()
        except:
            pass


def worker( p_fileList, p_i):
    t_finslStatus='SUCCESS'
    p_fileList.setCommandStatus('RUNNING',p_i)
    t_logName='/tmp/' + p_fileList.fileList[p_i]['fileName']
    t_configFile = p_fileList.fileList[p_i]['configFile']
    t_configFileH = open(t_configFile,"r")
    t_tmpConfigFile = "/tmp/feed."+ p_fileList.fileList[p_i]['fileType']+"_"+str(random.randint(1,1000))+"_tmp.conf"
    t_tmpConfigFileH = open(t_tmpConfigFile,"w")
    #yes i know it is duplicated
    for t_configFileLine in t_configFileH:
        if re.search('actions',t_configFileLine) : t_tmpConfigFileH.write(t_configFileLine)
        for block in p_fileList.fileList[p_i]['blocks']:
            if block['blockRerun']:
                if re.search(r''+block['blockName'].rstrip()+'\.[a-zA-Z]',t_configFileLine): t_tmpConfigFileH.write(t_configFileLine)
    t_tmpConfigFileH.flush()
    t_tmpConfigFileH.close()
    try:
        with open(t_logName,"w") as f:
            f.flush()
            t_command = p_fileList.fileList[p_i]['command'] + ' --external-config ' + t_tmpConfigFile
            os.system("echo " + t_command + " >> /tmp/log")
            subprocess.run(t_command.split(), check=True, stdout=f, stderr=subprocess.STDOUT)
            #subprocess.run(["ls", "-l"], check=True, stdout=f, stderr=subprocess.STDOUT)

    except subprocess.CalledProcessError as err:
        p_fileList.setCommandStatus('ERROR',p_i)
        return err

    with open(t_logName,"r") as f:
        for t_line in f:
            if re.search('ERROR',t_line) : t_finslStatus='ERROR'
    p_fileList.setCommandStatus(t_finslStatus,p_i)
    os.remove(t_logName)


def drawWorkArea(stdscr):
    l_buttonPressed = 0
    l_selectedItem = 0
    l_currentSearchString=''

    o_files = fileList([4,4],os.environ['REP_APP_LOG_DIR']+'/mft.*.log')
    o_search = searchField([2,2])
    o_confBox = confirmationBox([6,6] )
    o_objectList = Object()

    curses.start_color()
    curses.curs_set(0)
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(3, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_YELLOW, curses.COLOR_BLACK)

    curses.init_pair(42, curses.COLOR_RED, curses.COLOR_WHITE)
    curses.init_pair(52, curses.COLOR_GREEN, curses.COLOR_WHITE)


    while True:
        if ( l_buttonPressed == ord('q') ): break # and o_objectList.getActive() != 'search'): break
        stdscr.clear()
        t_scrY, t_scrX = stdscr.getmaxyx()
        o_files.maxLines=10
        o_files.maxCols=t_scrX
        o_objectList.processButton(l_buttonPressed,stdscr,o_files, o_confBox)
        o_objectList.setActiveObject(o_search, o_files, o_confBox)
        o_search.processButton(l_buttonPressed)
        o_files.processButton(l_buttonPressed)
        o_confBox.processButton(l_buttonPressed,o_objectList,o_files)
        if o_search.is_active: o_files.setVisible(o_search.searchString)
        o_search.draw(stdscr)
        o_files.draw(stdscr)
        o_confBox.draw(stdscr, o_files)
        #o_confBox.activeBox='RunningBox'
        #    o_objectList.active=0
        #    o_objectList.setActiveObject(o_search, o_files, o_confBox)

        #begin_x = 40; begin_y =39
        #height = 5; width = 40
        #win = curses.newwin(height, width, begin_y, begin_x)
        #rectangle(stdscr, 0,0, begin_y+2, begin_x+2)
        #win.addstr("test")

        debug = "button: {}, Active: {}, Search: {} , tmp: {}".format(l_buttonPressed, o_objectList.getActive(), o_objectList.searchString, o_confBox.activeBox)
        stdscr.addstr(0, 0 , debug, curses.color_pair(2))
        stdscr.refresh()
        o_confBox.refresh()
        l_buttonPressed = stdscr.getch()

def main():
    curses.wrapper(drawWorkArea)

if __name__ == "__main__":
    main()

# vim: set ts=4 sts=4 et sw=4 ft=python:
