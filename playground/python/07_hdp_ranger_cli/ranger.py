import requests
import logging
import json
import random
import argparse
from pprint import pprint

logging.basicConfig(filename="log",
        format="%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)-4d] %(message)s",
        datefmt="%Y-%m-%d:%H:%M:%S",
        level=logging.WARNING)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

u="admin"
p="DARTH_password2"
h="h1:6080"

class Ranger:
    def __init__(self,host,user,password):
        self._host=host
        self._user=user
        self._password=password
        self._url=self._check_protocol()

    def _get(self,url):
        req = self._url+url
        headers = {"Content-Type": "application/json", "Accept":"application/json"}
        logger.debug(f"In self._get, req: {req}")
        resp = requests.get(req, headers=headers, auth=(self._user,self._password))
        if resp:
            return resp
        else:
            logger.warning(f"Get request unsuccessful. Message: {resp.text}")
            return resp

    def _post(self,url,data):
        req = self._url+url
        headers = {"Content-type": "application/json", "Accept":"application/json"}
        logger.debug(f"In self._post, req: {req} data: {data}")
        resp=requests.post(req, data=data,headers=headers, auth=(self._user,self._password))
        if resp:
            return resp
        else:
            logger.warning(f"Post request unsuccessful. Message: {resp.text}")
            return resp

    def _put(self,url,data):
        req = self._url+url
        headers = {"Content-type": "application/json", "Accept":"application/json"}
        logger.debug(f"In self._put, req: {req} data: {data}")
        resp=requests.put(req, data=data,headers=headers, auth=(self._user,self._password))
        if resp:
            return resp
        else:
            logger.warning(f"Put request unsuccessful. Message: {resp.text}")
            return resp

    def _delete(self,url):
        req = self._url+url
        headers = {"Content-type": "application/json", "Accept":"application/json"}
        logger.debug(f"In self._delete, req: {req}")
        resp=requests.delete(req,headers=headers, auth=(self._user,self._password))
        if resp:
            return resp
        else:
            logger.warning(f"Delete request unsuccessful. Message: {resp.text}")
            return resp

    def _check_protocol(self):
        # to determine if HTTP or HTTPS
        try:
            url="https://"+self.__host+"/service/users"
            logger.debug("Trying HTTPS connection to: "+url)
            resp = requests.get(url,auth=(self._user,self._password))
            resp.raise_for_status()
        except requests.exceptions.HTTPError as http_err:
            logger.error(http_err)
        except Exception as err:
            url="http://"+self._host+"/service/users"
            logger.warning("HTTPS connection failed, trying HTTP: " +url)
            try:
                resp = requests.get(url,auth=(self._user,self._password))
                resp.raise_for_status()
            except requests.exceptions.HTTPError as http_err:
                logger.error(http_err)
            except Exception as err:
                logger.error(err)
            else:
                logger.info("Discovered protocol: HTTP");
                return "http://"+self._host+"/service"
        else:
            logger.info("Discovered protocol: HTTPS");
            return "https://"+self._host+"/service"


    def addUser(self,userName):
        BBSGroupId=self.getBBSGroupId();
        data={}
        data['name']=userName
        data['firstName']=""
        data['lastName']=""
        data['loginId']=userName
        data['password']="Z"+str(random.randint(100000000,99999999999999))
        data['emailAddress']=""
        data['description']="User added based on BBS rights"
        data['groupIdList']=[BBSGroupId]
        data['status']=1
        data['isVisible']=1
        data['userRoleList']=['ROLE_USER']
        data['userSource']=1
        resp = self._post("/xusers/secure/users",json.dumps(data))
        return resp

    def addUserToPolicy(self,userName,policyId,policyName,serviceName,modelUser):
        logger.info(f"Adding user:{userName} to policy id:{policyId}, service name:{serviceName}, policy name:{policyName}")
        resp = self._get("/public/v2/api/policy/"+str(policyId))
        policy = resp.json()
        for policyItem in policy['policyItems']:
            if ( modelUser in policyItem['users'] ):
                policyItem['users'].append(userName)
        resp = self._put("/public/v2/api/policy/"+str(policyId),json.dumps(policy))

    def applyUserPermBaseOnModel(self,userName,modelUser):
        logger.info(f"Checking user:{userName} policy against model user:{modelUser}")
        resp = self._get("/public/v2/api/policy")
        policies = resp.json()
        for policy in policies:
            #check each policy item if item users has either username or modeluser
            for policyItem in policy['policyItems']:
                #pprint(policyItem['users'])
                if ( modelUser in policyItem['users'] and userName in policyItem['users'] ) :
                    logger.debug(f"policy: {policy['name']}, service: {policy['service']} -> model user: {modelUser} YES, user:{userName} YES ->  No change needed")
                elif ( modelUser in policyItem['users'] and userName not in  policyItem['users'] ) :
                    logger.debug(f"policy: {policy['name']}, service: {policy['service']} -> model user: {modelUser} YES, user:{userName} NO -> Adding user to policy")
                    self.addUserToPolicy(userName,policy['id'],policy['name'], policy['service'] ,modelUser)
                elif ( modelUser not in policyItem['users'] and userName  in  policyItem['users'] ) :
                    logger.debug(f"policy: {policy['name']}, service: {policy['service']} -> model user: {modelUser} NO, user:{userName} YES -> Removing user from policy")
                    self.deleteUserFromPolicy(userName, policy['id'],policy['name'],policy['service'])


    def deleteUser(self,userName):
        #resp=self._delete("/xusers/users/userName/"+userName)
        resp=self._delete("/xusers/secure/users/"+userName)
        return resp

    def deleteUserFromPolicy(self,userName,policyId,policyName,serviceName):
        logger.info(f"Deleting user:{userName} from policy id:{policyId}, service name:{serviceName}, policy name: {policyName}")
        resp = self._get("/public/v2/api/policy/"+str(policyId))
        policy = resp.json()
        for policyItem in policy['policyItems']:
            if ( userName in policyItem['users'] ):
                policyItem['users'].remove(userName)
        resp = self._put("/public/v2/api/policy/"+str(policyId),json.dumps(policy))


    def enableUser(self,userName):
        resp = self._get("/xusers/users/userName/"+userName)
        data = {}
        data=resp.json()
        id=str(data['id'])
        data['isVisible']=1
        #pprint(data)
        resp = self._put("/xusers/secure/users/"+id,json.dumps(data))
        return resp


    def getBBSGroupId(self,BBSGroupName='BBS'):
        logger.debug(f"Getting {BBSGroupName} group id")
        resp = self._get("/xusers/groups/groupName/"+BBSGroupName)
        if resp:
            logger.debug(f"Grou {BBSGroupName} exists")
            return resp.json()['id']
        else :
            logger.debug(f"Group {BBSGroupName} not found, adding")
            data={}
            data['description'] = "Users based on BBS rights"
            data['groupSource'] = 0
            data['groupType'] = 0
            data['isVisible'] = 1
            data['name'] =  BBSGroupName
            resp = self._post("/xusers/groups",json.dumps(data))
            return resp.json()['id']

    def getUsersForGroupName(self,groupName):
        logger.debug(f"Getting {groupName} group id")
        resp = self._get("/xusers/groups/groupName/"+groupName)
        userList = []
        if resp:
            logger.debug(f"Grou {groupName} exists")
            resp2 = self._get("/xusers/"+str(resp.json()['id'])+"/users").json()
            #pprint(resp2)
            for user in resp2['vXUsers']:
                userList.append({'userId': user['id'], 'userName': user['name'], 'isVisible':user['isVisible']})
            return userList
        else:
            logger.error(f"Group {groupName} does not exists")
            return userList


    def isUserEnabled(self,userName):
        resp = self._get("/xusers/users/userName/"+userName)
        data = resp.json()
        return data['isVisible']

    def userExists(self,userName):
        logger.debug(f"Checking if user: {userName} exists")
        resp = self._get("/xusers/users/userName/"+userName)
        logger.debug(f"Response:{resp}, content: {resp.text}")
        return resp



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Tool syncing Ranger with BBS rights')
    parser.add_argument("--bbsfile", type=str, help="BBS file", default='bbs.csv')
    args=parser.parse_args()
    logger.info(f"Opening BBS file: {args.bbsfile}")
    # read BBS file and load it to dictionary
    usersFromBBSFileRaw = open(args.bbsfile,'r') .read().splitlines();
    usersFromBBSFile = []
    for rawLine in usersFromBBSFileRaw :
        usersFromBBSFile.append({'userName':rawLine.split(',')[0], 'modelUser':rawLine.split(',')[1]})
    logger.info(f"BBS file red, no records found: {len(usersFromBBSFile)}")
    logger.debug(f"usersFromBBSFile :{usersFromBBSFile}")

    #init ranger object
    ranger=Ranger(h,u,p)
    logger.info(f"Removing users that in ranger and are not in BBS file")
    usersFromRanger = ranger.getUsersForGroupName('BBS')
    logger.debug(f"usersFromRanger: {usersFromRanger}")
    for rangerUser in usersFromRanger:
        if not any(rangerUser['userName'] == bbsUser['userName'] for bbsUser in usersFromBBSFile):
            if rangerUser['isVisible']:
                logger.info(f"Ranger user: {rangerUser['userName']} not in BBS... Removing")
                ranger.deleteUser(rangerUser['userName'])

    for bbsUser in usersFromBBSFile:
        if not any(bbsUser['userName'] == rangerUser['userName'] for  rangerUser in usersFromRanger):
            logger.info(f"BBS user: {bbsUser['userName']} not in Ranger... Adding")
            ranger.addUser(bbsUser['userName']);
        elif not ranger.isUserEnabled(bbsUser['userName']):
            logger.info(f"BBS user: {bbsUser['userName']} exists in Ranger but disabled... Enabling")
            ranger.enableUser(bbsUser['userName'])

        ranger.applyUserPermBaseOnModel(bbsUser['userName'],bbsUser['modelUser'])


    #remove all users that are in ranger and are not in BBS file
    #add all users that are in BBS and are not in 


    #pprint(ranger._get("/xusers/23/users").json())
    #pprint(ranger._get("/public/v2/api/policy/1").json())
    #ranger.addUser("libo1")
    #print(ranger.deleteUser("libo4"))
    #print(ranger.isUserEnabled("libo2"))
    #print(ranger.enableUser("libo2"))
    #print(ranger.isUserEnabled("libo2"))
    #ranger.applyUserPermBaseOnMode("libo3","mu-it-ro")
    #pprint(ranger.getUsersForGroupName('BBS'))

