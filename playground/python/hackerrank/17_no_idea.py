n,m = map(int, input().split())
inSet = list(map(int, input().split()))
A_array = list(map(int, input().split()))
A = set(A_array)
B_array = list(map(int, input().split()))
B = set(B_array)
h = 0

A_min = A.difference(B)
B_min = B.difference(A)

for n in inSet:
    if n in A_min : h = h + 1
    if n in B_min : h = h - 1

print(h)
