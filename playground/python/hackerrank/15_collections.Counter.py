from collections import Counter

def merge_the_tools(string, k):
    # your code goes here
    str_len=len(string)
    parts = str_len // k
    part_list=[]
    for part in range(parts):
        part_list.append(string[part*k:part*k+k])
    #print(f'{part_list}')
    for part in part_list:
        seen_list=[]
        tmp_string=''
        for char_pos in range(k):
            if part[char_pos] not in seen_list:
                seen_list.append(part[char_pos])
                tmp_string+=part[char_pos]
        print(tmp_string)

if __name__ == '__main__':
    #10
    #2 3 4 5 6 8 7 6 5 18
    #6
    #6 55
    #6 45
    #6 55
    #4 40
    #18 60
    #10 50
    #string, k = input(), int(input())
    no_shoes=int(input())
    sizes = Counter(input().split(' '))
    no_customers = int(input())
    total_sum=0
    for i in range(0,no_customers):
        size,price = input().split(' ')
        price = int(price)
        print(f"size: {size}, price:{price}, sizes[size]:{sizes[size]}")
        if sizes[size] > 0:
            print(f"in size {sizes[size]}")
            total_sum+=price
            sizes[size]-=1
    print(total_sum)
