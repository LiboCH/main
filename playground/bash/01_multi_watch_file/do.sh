#!/bin/bash
set -u 
set -o pipefail

usage () {
cat <<EOF
Usage: $0 [options]
        -s proto://user@host:/dir/file 
        [-s "proto[ proto options]://user@host:/dir/file" ]
        -d dest directory
        -n no of files min:max ex 0:1, 1:1 , 1:n
        [-t timeout ]
EOF
}

cmdCore=()
fileToGet=()
filesFound=0
cmdIndex=0
unique=$(date +%Y%m%d_%H%M%S.%N)
cmdErrFile="/tmp/err.${unique::19}"
cmdOutFile="/tmp/out.${unique::19}"
filesFoundList="/tmp/filelsit.${unique::19}"
sleepSec=3
pullStatus=0

cleanUpPartialDownload () {
    while read $file ; do
        [[ -f ${dest}/$(basename $file) ]] && rm -rm ${dest}/$(basename $file)
    done < $filesFoundList
}


scpCheck (){
    i=$1
    lCmd=$(echo ${cmdCore[$i]} | sed -e "s/scp /ssh /"  ) 
    lCmd="$lCmd ls -1 ${fileToGet[$i]} 2> $cmdErrFile 1> $cmdOutFile"
    echo "scpCheck | doing: $lCmd"
    out=$(eval $lCmd )
    if [[ $? -ne 0 ]] ; then
        echo "scpCheck | WARN: command failed:"
        cat $cmdErrFile
    else
        filesFound=$(wc -l < $cmdOutFile)
        echo "scpCheck | Found files: $filesFound" 

    fi
}

scpGet (){
    i=$1
    while read file ; do
        lCmd="${cmdCore[$i]}:$file $dest 2> $cmdErrFile 1> $cmdOutFile"
        echo "scpGet | $lCmd"
        out=$(eval $lCmd )
        if [[ $? -ne 0 ]] ; then
            echo -n "scpGet | ERROR:"
            cat $cmdErrFile
            cleanUpPartialDownload
            exit 1
        fi
    done < $filesFoundList
    echo "scpGet | copy succesfull , renaming source files "
    while read file ; do
        lCmd=$(echo ${cmdCore[$i]} | sed -e "s/scp /ssh /"  ) 
        #< /dev/null as that ssh b...h eats my arguments
        lCmd="$lCmd mv $file $file.${unique::19} 2> $cmdErrFile 1> $cmdOutFile < /dev/null"
        echo "scpGet | $lCmd"
        out=$(eval $lCmd )
        if [[ $? -ne 0 ]] ; then
            echo -n "scpGet | ERROR:"
            cat $cmdErrFile
            exit 1
        fi
    done < $filesFoundList
    pullStatus=1


}
sftpCheck(){
    i=$1
    lCmd="echo \"ls -1 ${fileToGet[$i]}\" | ${cmdCore[$i]} 2> $cmdErrFile 1> $cmdOutFile"
    echo "sftpCheck | doing: $lCmd"
    out=$(eval $lCmd )
    if [[ $? -ne 0 ]] ; then
        echo "sftpCheck | WARN: command failed:"
        cat $cmdErrFile
    else
        egrep -v "^sftp" $cmdOutFile > $cmdOutFile.tmp
        mv $cmdOutFile.tmp $cmdOutFile
        filesFound=$(wc -l < $cmdOutFile)
        echo "sftpCheck | Found files: $filesFound" 
    fi
}

sftpGet() {
    i=$1
    while read file ; do
        lCmd="echo \"get $file $dest\" | ${cmdCore[$i]} 2> $cmdErrFile 1> $cmdOutFile"
        echo "sftpGet | $lCmd"
        out=$(eval $lCmd )
        if [[ $? -ne 0 ]] ; then
            echo -n "scpGet | ERROR:"
            cat $cmdErrFile
            cleanUpPartialDownload
            exit 1
        fi
    done < $filesFoundList
    echo "scpGet | copy succesfull , renaming source files "
    while read file ; do
        lCmd="echo \"rename $file $file.${unique::19}\" | ${cmdCore[$i]} 2> $cmdErrFile 1> $cmdOutFile"
        echo "sftpGet | $lCmd"
        out=$(eval $lCmd )
        if [[ $? -ne 0 ]] ; then
            echo -n "sftpGet | ERROR:"
            cat $cmdErrFile
            exit 1
        fi
    done < $filesFoundList
    pullStatus=1

}

parseSource (){
    pSource=$1
    echo "parseSource | $pSource"

    #split regexp for easy invesitgation in future 
    #1) check for protocol
    if [[ "$pSource" =~ ^(scp|sftp)[\[|:].*$ ]] ; then 
        lProto=${BASH_REMATCH[1]}
        echo "parseSource | proto: ${BASH_REMATCH[1]}"
    else
        echo "parseSource | unable to get protoclol from source string: $pSource"
        exit 1
    fi

    #2) check for protocol options 
    lProtoParam=""
    [[ "$lProto" == "scp" ]] && lProtoParam="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o PasswordAuthentication=no"
    [[ "$lProto" == "sftp" ]] && lProtoParam="-o PasswordAuthentication=no -b -"
    if [[ "$pSource" =~ ^$lProto\[(.*)\] ]] ; then 
        lProtoParam=${BASH_REMATCH[1]}
        echo "parseSource | proto param: ${BASH_REMATCH[1]}"
    else
        echo "parseSource | no proto param detected, using: $lProtoParam"
    fi
    
    #3) extract username@host
    lUserAtHost=$(echo $pSource | cut -d: -f2 | sed -e "s/^\/\///")
    if [[ ! "$lUserAtHost" =~ ^.*@.*$ ]] ; then 
        echo "Uanbe to find valid username@host in string: $lUserAtHost"
    fi
    
    lFile=$(echo $pSource | cut -d: -f3)
    [[ -z "$lFile" ]] && { echo "Unable to dedermine file to downlad in string: $pSource" ; exit ;} 
    #4) extract file/file mast to pull
    cmdCore+=("$lProto $lProtoParam $lUserAtHost")
    fileToGet+=("$lFile")

}

sources=()
dest=""
noOfFile=""
timeout=1
while getopts ":s:d:n:t:" opt; do
    case $opt in
        s) sources+=( "$OPTARG" );;
        d) dest="$OPTARG";;
        n) noOfFiles="$OPTARG";;
        t) timeout="$OPTARG";;
        *) echo "Error unknown option -$OPTARG"
            usage
            exit 1
       ;;
    esac
done
[[ ${#sources[@]} -eq 0 ]] && echo "Minimum  1 source is required ( -s ) " && usage && exit 1
[[ -z "$dest" ]] &&  echo "Missing destination ( -d )" && usage  && exit 1

minFiles=1
maxFiles=999

[[ !  -z "$(echo $noOfFiles | cut -d: -f1)" ]] && minFiles=$(echo $noOfFiles | cut -d: -f1)
[[ !  -z "$(echo $noOfFiles | cut -d: -f2)" && "$(echo $noOfFiles | cut -d: -f2)" != "n" ]] && maxFiles=$(echo $noOfFiles | cut -d: -f2)
echo "main | minFiles: $minFiles, maxFiles: $maxFiles"

commands=()
for i in "${sources[@]}" ; do
    parseSource "$i"
done
timeoutCounter=0
while [[ $timeoutCounter -lt $timeout ]] ; do
    timeoutCounter=$(( $timeoutCounter + $sleepSec ))
    for i in "${!cmdCore[@]}" ; do
        filesFound=0
        check="$(echo ${cmdCore[$i]} | cut -d" " -f1)Check"
        $check $i
        if [[ $filesFound -ne 0 && $filesFound -ge $minFiles && $filesFound -le $maxFiles ]] ; then
            cmdIndex=$i
            cp $cmdOutFile $filesFoundList 
            echo "main | Found enough files matching criteria"
            break
        fi
    done
    if [[ -f $filesFoundList ]] ; then 
        get="$(echo ${cmdCore[$cmdIndex]} | cut -d" " -f1)Get"
        $get $cmdIndex
    else
        echo "main |  Required files not found, seconds till timeout $(($timeout - $timeoutCounter )),  sleeping for $sleepSec"
        [[ $timeoutCounter -ge $timeout ]] && break 
        sleep $sleepSec
    fi
    [[ $pullStatus -eq 1  ]] && break 
done

rm -rf $cmdErrFile
rm -rf $cmdOutFile
rm -rf $filesFoundList

[[ $pullStatus -ne 1 ]] && echo "ERROR: expected files not found, time-out reached." && exit 1 
exit 0
